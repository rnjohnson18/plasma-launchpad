# Plasma Launchpad
This is the Plasma Launchpad, an application launcher plasmoid that is intended to replicate the Launchpad from macOS. 
Plasma Launchpad is based off of the [UMenu plasmoid](https://gitlab.com/divinae/umenu), which in turn is based off of the [Simple Menu plasmoid](https://store.kde.org/p/1169537/).

### Features
- Fullscreen blurred background
- Scrollable pages
- Folders [*work in progress*]
- Icons that scale by resolution